from __future__ import absolute_import, division, print_function
from ansible.plugins.inventory import BaseInventoryPlugin
from ansible.inventory.group import to_safe_group_name
from ansible.errors import AnsibleParserError
import os
import logging
import json
import snipeit
import yaml

# https://developers.redhat.com/blog/2021/03/10/write-your-own-red-hat-ansible-tower-inventory-plugin

# SnipeIT field with variable overwrites
# "additionalAttributes": "var1: val1\r\nvar2: val2",
ANSIBLE_VARIABLES_FIELD = "additionalAttributes"

__metaclass__ = type

ANSIBLE_METADATA = {
    "metadata_version": "1.0.0",
    "status": ["preview"],
    "supported_by": "",
}
DOCUMENTATION = r"""
    module: let.snipeit_inventory.plugins.inventory.snipeit_plugin
    plugin_type: inventory
    short_description: Returns Ansible inventory from SnipeIT
    description: Returns Ansible inventory from SnipeIT.
    requirements:
        - snipeit >= 1.1
    options:
        plugin:
            description: Name of the plugin
            required: true
            choices: ['let.snipeit_inventory.snipeit_plugin']
        snipeit_url:
            description: Base URL of a SnipeIT server
            required: true
        key_file:
            description: File name containing the API key to access SnipeIT (set/override with env variable SNIPEIT_API_KEY)
            required: false
"""
EXAMPLES = """
# my.snipeit.yml
plugin: ethz.snipeit_inventory.snipeit_plugin
snipeit_url: http://localhost:2222
"""


class InventoryModule(BaseInventoryPlugin):
    NAME = "let.snipeit_inventory.snipeit_plugin"

    def __init__(self):
        BaseInventoryPlugin.__init__(self)
        self.api_key = None
        self.url = None
        self.key_file = None
        self.use_cache = None
        self._config_data = None

    def verify_file(self, path):
        """Return true/false if this is possibly a valid file for this plugin to
        consume
        """
        return True

    def _get_hosts(self):
        # f = open("/home/bgiger/hostlist.json")
        # return json.load(f)

        a = snipeit.Assets()
        hosts = a.get(server=self.url, token=self.api_key)
        if hosts is None:
            raise ConnectionError("No hosts could be read")
        return json.loads(hosts)

    def _add_group(self, fieldname):
        groupname = to_safe_group_name(fieldname, force=True)
        if groupname != "" and groupname not in self.inventory.groups:
            self.inventory.add_group(groupname)
        return groupname

    def _populate(self):
        for host in self._get_hosts()["rows"]:
            hostname = host["asset_tag"]
            self.inventory.add_host(hostname)
            # Regular fields
            self.inventory.set_variable(hostname, "notes", host["notes"])
            # Custom fields
            for fieldname in host["custom_fields"].keys():
                # Field 'Ansible' is special, used to construct groups
                if fieldname == "Ansible":
                    if host["custom_fields"]["Ansible"]["value"] is not None and len(host["custom_fields"]["Ansible"]["value"])>0:
                        for ansible_tag in host["custom_fields"]["Ansible"][
                            "value"
                        ].split(","):
                            # is i.e. {'field': '_snipeit_ansible_7', 'value': 'someValue', 'field_format': 'ANY'}
                            # or 'value': 'tag1, tag2' or 'value': None
                            self.inventory.add_child(
                                self._add_group(ansible_tag.strip()), hostname
                            )
                # freetext variable overwrite field
                if fieldname == ANSIBLE_VARIABLES_FIELD:
                    # {'field': '_snipeit_additionalattributes_8', 'value': 'var1: val1\r\nvar2: val2', 'field_format': 'ANY'}
                    if (
                        host["custom_fields"][ANSIBLE_VARIABLES_FIELD]["value"]
                        is not None
                    ):
                        # Field is in YAML format, parse and attach to host
                        vars = yaml.safe_load(
                            host["custom_fields"][ANSIBLE_VARIABLES_FIELD]["value"]
                        )
                        if vars is not None:
                          for var_name in vars.keys():
                              self.inventory.set_variable(
                                  hostname, var_name, vars[var_name]
                              )

                # Other fields just get stored in variables
                else:
                    # print(hostname, fieldname, host["custom_fields"][fieldname]["value"])
                    self.inventory.set_variable(
                        hostname, fieldname, host["custom_fields"][fieldname]["value"]
                    )

            # Grouping fields
            self.inventory.add_child(
                self._add_group(str(host["category"]["name"])), hostname
            )
            try:
                self.inventory.add_child(
                    self._add_group(str(host["custom_fields"]["Umgebung"]["value"])),
                    hostname,
                )
            except:  # noqa: E722
                pass
            try:
                self.inventory.add_child(
                    self._add_group(str(host["custom_fields"]["Service"]["value"])),
                    hostname,
                )
            except:  # noqa: E722
                pass

    def parse(self, inventory, loader, path, cache=True):
        """Return dynamic inventory from SnipeIT"""
        BaseInventoryPlugin.parse(self, inventory, loader, path, cache)
        # Read config file
        self._config_data = self._read_config_data(path)
        msg = ""
        if not self._config_data:
            msg = "File empty. This is not my config file"
        elif "plugin" in self._config_data and self._config_data["plugin"] != self.NAME:
            msg = "plugin config file, but not for us: %s" % self._config_data["plugin"]
        elif "plugin" not in self._config_data:
            msg = "it's not a plugin configuration"
        if msg:
            raise AnsibleParserError(msg)

        # Mandatory configuration items
        try:
            self.url = self.get_option("snipeit_url")
        except Exception as e:
            raise AnsibleParserError("All correct options required: {}".format(e))

        # Optional items
        #
        # Try to read API key file from configuration
        try:
            self.key_file = self.get_option("key_file")
        except KeyError:
            pass
        # Key file in env variable overwrites configuration
        self.key_file = os.getenv("SNIPEIT_API_KEY_FILE", self.key_file)
        if self.key_file:
            # Try to read API key from flie
            try:
                f = open(self.key_file)
                self.api_key = f.read().rstrip()
                f.close()
            except FileNotFoundError:
                logging.info("API key file not found, falling back to ENV variable")
        # Finally, an env variable would have precedence
        self.api_key = os.getenv("SNIPEIT_API_KEY", self.api_key)
        if self.api_key is None:
            logging.error("No API key for SnipeIT found, exiting")
            exit(1)

        # Call our internal helper to populate the dynamic inventory
        self._populate()
