# Ansible Collection - let.snipeit_inventory

Custom built inventory for LET SnipeIT asset management.

# Installation
Not final, for initial development only:

```shell
pip install snipeit
ansible-galaxy collection install git@gitlab.ethz.ch:ansible-let/snipeit_inventory.git
# or
# ansible-galaxy collection install git+https://gitlab.ethz.ch/ansible-let/snipeit_inventory.git
```

# Configuration
Create a file `inventory.yml`with:

```yaml
plugin: "let.snipeit_inventory.snipeit_plugin"
snipeit_url: "https://snipeit-let.ethz.ch"
key_file: /insert/file/here
```

The `key_file` may contain the API key to access SnipeIT.

## Env Variables

As an alternative, the API key may also be set as environment variable:

```shell
export SNIPEIT_API_KEY=abcdefg
```

Or use a variable to refer to a key file:

```shell
export SNIPEIT_API_KEY_FILE=/home/user/.ansible/snipeit_key
```


# Usage
Some of the attributes from SnipeIT are used for grouping, some are attached as variables, per host.
## Groups
- Asset tag
- Service
- _Ansible_ attributes (custom field checkboxes)
- _Umgebung_ (Development, Production etc)

## Variables
- `Funktion`
- `Hauptverantwortlicher`,
- `Nebenverantwortlicher`
- `additionalAttributes`
- `notes`
- `additionalAttributes`

The multiline field `additionalAttributes` is treated different: it must contain valid Ansible (or YAML)
variables, one variable per line:

```
ansible_variable1: value1
ansible_variable2: true
```

Because boolean values are only recognized in YAML inventories, the latter variable has to be tested as string:

```
when: ansible_variable|lower == "true"
```

# Useful Commands

- List of inventory plugins: `ansible-doc -t inventory -l`
- Inventory plugin help: `ansible-doc -t inventory let.snipeit_inventory.snipeit_plugin`
- Test inventory: `ansible-inventory --list -i inventory.yml`
